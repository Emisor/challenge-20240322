## Prueba Técnica Beeping

Una sencilla aplicación Web desarrollada en Laravel 10 usando varias herramientas de su ecosistema.

### Requisitos

- PHP 8.2.0+
- Node.js 18.0+
- Composer 2.0+
- MySQL (otros motores de bases de datos SQL tambien sirven)

### Instrucciones para levantar un entorno local

- Clonar este repositorio, ejecutando el siguiente comando en una terminal creada desde la ubicación deseada:

```git clone https://gitlab.com/emisor/challenge-20240322.git {carpeta-repo}```
- Moverse a la carpeta del repositorio: ```cd {carpeta-repo}```
- Copiar archivo .env.example, renombrar copia a .env, y configurar las siguientes variables:
```
APP_URL=http://127.0.0.1:8000 # (En caso de usar php artisan serve, sino configurar acorde a su entorno)
APP_ENV=local # (Opcional: Cambiar a "production" si desea implementar esta aplicación en un entorno productivo)
DB_DATABASE= # (Nombre de la base de datos que desee. Si no existe, más adelante la aplicación solicitará permiso para crearla)
REDIS_CLIENT=predis # (Opcional: usar "phpredis" si su entorno es compatible para mejor rendimiento)
APP_DEBUG=false # (Opcional: "true" activa el modo de depuración de errores, en caso de desearlo)
```
- Ejecutar los siguientes comandos:
```
composer install
npm install
npm run build
php artisan key:generate
php artisan migrate --seed
```

### Instrucciones correr las tareas en segundo plano

* Para una prueba inicial de corto plazo, ejecutar en terminales independientes en la carpeta del repositorio los siguientes comandos:

```
php artisan schedule:work
php artisan horizon:work
```
* Si desea mantener permanentemente estas funcionalidades activas, deberá seguir las instrucciones de la documentación de Laravel indicadas a continuación:
  - https://laravel.com/docs/10.x/scheduling#running-the-scheduler
  - https://laravel.com/docs/10.x/horizon#deploying-horizon


