<?php

use App\Models\Order;
use Livewire\Volt\Component;

new class extends Component {
    public $selectedOrderId;

    public function show($order)
    {
        $this->order = $order;
    }

    public function with(): array
    {
        return [
            'orders' => Order::orderBy('id', 'desc')->paginate(10),
        ];
    }

}; ?>

<div>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Orden</th>
                    <th>Cliente</th>
                    <th class="text-end pe-3">Total</th>
                </tr>
            </head>
            <tbody>
                @foreach($orders as $order)
                <tr>
                    <td>
                        {{ $order->order_ref }}
                    </td>
                    <td>
                        {{ $order->customer_name }}
                    </td>
                    <td class="text-end">
                        <div x-data="{ open: false }">
                            <span x-show="open" x-transition.opacity>
                                <table class="table table-borderless">
                                    <tbody>
                                        @foreach($order->orderLines as $product)
                                        <tr>
                                            <td class="text-end">
                                                {{ $product->name }}
                                            </td>
                                            <td style="width: 60px;">
                                                x {{$product->pivot->qty }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </span>
                            <div>
                                $ {{ sprintf("%0.2f", $order->total) }}
                                <button @click="open = !open" class="btn btn-link">
                                    <i class="bi" :class="{'bi-eye' : !open, 'bi-eye-slash' : open}"></i>
                                </button>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>
        {{ $orders->links() }}

</div>
